package com.mecbey.nemesisbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mecbey.nemesisbackend.entity.Tweets;

public interface TweetRepository extends JpaRepository<Tweets, Long>  {
	Tweets getByName(String tweet);

}
