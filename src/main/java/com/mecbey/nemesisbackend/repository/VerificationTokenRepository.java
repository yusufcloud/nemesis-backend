package com.mecbey.nemesisbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mecbey.nemesisbackend.entity.VerificationToken;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, String> {
    List<VerificationToken> findByUserEmail(String email);
    List<VerificationToken> findByToken(String token);
}