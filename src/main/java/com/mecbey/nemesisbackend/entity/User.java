package com.mecbey.nemesisbackend.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "users2", indexes = {@Index(name = "idx_email", columnList = "email")})
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	
	@Column(name = "first_name", length = 100)
	String firstName;
	
	@Column(name = "last_name", length = 100)
	String lastName;
	
	@Column(name = "email", length = 100, unique = true)
	String email;
	
	@Column(name = "password", length = 200)
	String password;
	
	@Column(name = "isActive")
	private Boolean isActive = false;

}
