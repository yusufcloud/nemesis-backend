package com.mecbey.nemesisbackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "hashtags")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Hashtags {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	String id;
	
	@Column(name = "name", length = 100)
	String name;
	
	@Column(name = "createdAt", length = 100)
	String createdAt;
	
	@Column(name = "hashtags", length = 100)
	String hashtags;

}
