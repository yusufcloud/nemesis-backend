package com.mecbey.nemesisbackend.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class VerificationToken {

	public static final String STATUS_PENDING = "PENDING";
	public static final String STATUS_VERIFIED = "VERIFIED";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String token;
	private String status;
	private LocalDateTime expiredDateTime;
	private LocalDateTime issuedDateTime;
	private LocalDateTime confirmedDateTime;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id")
	private User user;

	public VerificationToken() {
		this.token = UUID.randomUUID().toString();
		this.issuedDateTime = LocalDateTime.now();
		this.expiredDateTime = this.issuedDateTime.plusDays(1);
		this.status = STATUS_PENDING;
	}

}
