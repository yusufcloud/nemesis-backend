package com.mecbey.nemesisbackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "project")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int userId;
	
	@Column(name = "title", length = 100)
	String title;
	
	@Column(name = "description", length = 100)
	String description;
	
	@Column(name = "createdAt", length = 100)
	String createdAt;

}
