package com.mecbey.nemesisbackend.api;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mecbey.nemesisbackend.dto.UserDto;
import com.mecbey.nemesisbackend.entity.User;
import com.mecbey.nemesisbackend.service.implement.UserServiceImplement;
import com.mecbey.nemesisbackend.util.ApiPaths;
import com.mecbey.nemesisbackend.util.TPage;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(ApiPaths.UserCtrl.CTRL)
@Api(value = ApiPaths.UserCtrl.CTRL, description = "User APIs")
@Slf4j
public class UserController {
	
	private final UserServiceImplement userServiceImplement;
	
	public UserController(UserServiceImplement userServiceImplement) {
		this.userServiceImplement = userServiceImplement;
	}
	
	@GetMapping("/pagination")
    @ApiOperation(value = "Get By Pagination Operation", response = UserDto.class)
    public ResponseEntity<TPage<UserDto>> getAllByPagination(Pageable pageable) {
        TPage<UserDto> data = userServiceImplement.getAllPageable(pageable);
        return ResponseEntity.ok(data);
    }
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Get By Id Operation", response = UserDto.class)
	public ResponseEntity<UserDto> getById(@PathVariable("id") Long id){
		log.info("UserController-> GetByID :");
		
		log.debug("UserController-> GetByID -> PARAM:" + id);
		UserDto userDto = userServiceImplement.getById(id);
		return ResponseEntity.ok(userDto);
	}
	
	@PostMapping
	@ApiOperation(value = "Create Operation", response = UserDto.class)
	public ResponseEntity<UserDto> createUser(@Valid @RequestBody User user){
		return ResponseEntity.ok(userServiceImplement.save(user));
	}
	
	@PutMapping("/{id}")
	@ApiOperation(value = "Update Operation", response = UserDto.class)
	public ResponseEntity<UserDto> updateUser(@PathVariable(value = "id", required = true) Long id, @Valid @RequestBody User user){
		return ResponseEntity.ok(userServiceImplement.update(id, user));
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Delete Operation", response = Boolean.class)
	public ResponseEntity<Boolean> deleteUser(@PathVariable(value = "id", required = true) Long id){
		return ResponseEntity.ok(userServiceImplement.delete(id));
	}

}
