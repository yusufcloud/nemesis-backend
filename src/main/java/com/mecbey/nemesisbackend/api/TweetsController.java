package com.mecbey.nemesisbackend.api;

import java.util.List;

import org.springframework.social.twitter.api.CursoredList;
import org.springframework.social.twitter.api.SearchParameters;
import org.springframework.social.twitter.api.SearchParameters.ResultType;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mecbey.nemesisbackend.service.implement.TweetServiceImplement;
import com.mecbey.nemesisbackend.util.ApiPaths;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(ApiPaths.TweetsCtrl.CTRL)
@Api(value = ApiPaths.TweetsCtrl.CTRL, description = "Tweet APIs")
public class TweetsController {

	private final TweetServiceImplement tweetServiceImplement;
	Twitter twitter = new TwitterTemplate("gqfBD3CdEfh8DAKqursOeewNi",
			"9YL9V3CloNd8KJxYuMZcL6SHzhwtl0t2Oz4anGBKUrf5BEFW52");

	public TweetsController(TweetServiceImplement tweetServiceImplement) {
		this.tweetServiceImplement = tweetServiceImplement;
	}

	@GetMapping("/{twitName}/{count}/{type}")
	public SearchResults getHashtag(@PathVariable("twitName") String twitName, @PathVariable("count") int count,
			@PathVariable("type") String type) {

		return twitter.searchOperations()
				.search(new SearchParameters(twitName).count(count).resultType(type.equals("recent") ? ResultType.RECENT
						: (type.equals("popular") ? ResultType.POPULAR : (type.equals("mixed") ? ResultType.MIXED : null))));
	}

	@GetMapping("/user/{userName}/{count}")
	public List<Tweet> getUser(@PathVariable("userName") String userName, @PathVariable("count") int count) {
		return twitter.timelineOperations().getUserTimeline(userName, count);
	}
	
	@GetMapping("/{twitName}/{count}")
	public CursoredList<TwitterProfile> getFollower(@PathVariable("twitName") String twitName, @PathVariable("count") long count,
			@PathVariable("type") String type) {

		return twitter.friendOperations().getFollowersInCursor(twitName, count);
	}

}
