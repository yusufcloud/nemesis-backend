package com.mecbey.nemesisbackend.api;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mecbey.nemesisbackend.dto.LoginRequest;
import com.mecbey.nemesisbackend.dto.RegistrationRequest;
import com.mecbey.nemesisbackend.dto.TokenResponse;
import com.mecbey.nemesisbackend.entity.User;
import com.mecbey.nemesisbackend.entity.VerificationForm;
import com.mecbey.nemesisbackend.repository.UserRepository;
import com.mecbey.nemesisbackend.security.JwtTokenUtil;
import com.mecbey.nemesisbackend.service.VerificationTokenService;
import com.mecbey.nemesisbackend.service.implement.UserServiceImplement;

@RestController
@RequestMapping("/api")
public class AccountController {

	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserServiceImplement userServiceImplement;

	@Autowired
	VerificationTokenService verificationTokenService;

	@GetMapping("/")
	public String index() {
		return "redirect:/email-verification";
	}

	@GetMapping("/email-verification")
	public String formGet(Model model) {
		model.addAttribute("verificationForm", new VerificationForm());
		return "verification-form";
	}

	@PostMapping("/email-verification")
	public String formPost(@Valid RegistrationRequest verificationForm) {
		return verificationTokenService.createVerification(verificationForm);
	}

	@GetMapping("/verify-email")
	@ResponseBody
	public String verifyEmail(String code) {
		return verificationTokenService.verifyEmail(code).getBody();
	}

	@PostMapping("/login/token")
	public ResponseEntity<TokenResponse> login(@Valid @RequestBody LoginRequest request) {
		authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
		final User user = userRepository.findByEmail(request.getEmail());
		final String token = jwtTokenUtil.generateToken(user);
		if (user.getIsActive() == true) {
			return ResponseEntity
					.ok(new TokenResponse(user.getEmail(), user.getFirstName(), user.getLastName(), token));
		} else {
			return null;
		}
	}

	@PostMapping("/register/token")
	public ResponseEntity<Boolean> register(@Valid @RequestBody RegistrationRequest registrationRequest)
			throws AuthenticationException {
		String result = formPost(registrationRequest);
		if (result.equals("")) {
			return ResponseEntity.ok(false);
		} else {
			return ResponseEntity.ok(true);
		}

	}

}
