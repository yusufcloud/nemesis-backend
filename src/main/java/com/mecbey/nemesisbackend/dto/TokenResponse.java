package com.mecbey.nemesisbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenResponse {
	private String email;
	private String firstName;
	private String lastName;
	private String token;

}
