package com.mecbey.nemesisbackend.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "User Data Transfer Object")
public class UserDto {
	@ApiModelProperty(value = "User ID")
	Long id;
	
	@ApiModelProperty(value = "First Name")
	@NotEmpty(message = "İsim bos birakilamaz.")
	private String firstName;
	
	@ApiModelProperty(value = "Last Name")
	@NotEmpty(message = "Soyisim bos birakilamaz.")
	private String lastName;
	
	@NotNull
	@ApiModelProperty(value = "E-Mail")
	@Email(message = "Hatali email")
	private String email;
	
	@ApiModelProperty(value = "isActive")
	private Boolean isActive;

}
