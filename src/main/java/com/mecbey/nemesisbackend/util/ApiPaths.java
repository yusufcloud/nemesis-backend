package com.mecbey.nemesisbackend.util;

public final class ApiPaths {
	private static final String BASE_PATH = "/api";
	
	public static final class UserCtrl{
		public static final String CTRL = BASE_PATH + "/user";
	}
	
	public static final class TweetsCtrl{
		public static final String CTRL = BASE_PATH + "/tweets";
	}

}
