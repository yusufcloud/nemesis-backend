package com.mecbey.nemesisbackend.service;

import com.mecbey.nemesisbackend.entity.Tweets;

public interface TweetService {
	
	Tweets getTweet(String twitName);

}
