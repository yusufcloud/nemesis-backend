package com.mecbey.nemesisbackend.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.mecbey.nemesisbackend.dto.RegistrationRequest;
import com.mecbey.nemesisbackend.entity.User;
import com.mecbey.nemesisbackend.entity.VerificationToken;
import com.mecbey.nemesisbackend.repository.UserRepository;
import com.mecbey.nemesisbackend.repository.VerificationTokenRepository;

@Service
public class VerificationTokenService {
	private UserRepository userRepository;
	private VerificationTokenRepository verificationTokenRepository;
	private SendingMailService sendingMailService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public VerificationTokenService(UserRepository userRepository,
			VerificationTokenRepository verificationTokenRepository, SendingMailService sendingMailService) {
		this.userRepository = userRepository;
		this.verificationTokenRepository = verificationTokenRepository;
		this.sendingMailService = sendingMailService;
	}

	public String createVerification(RegistrationRequest register) {
		try {
			User user;
			user = new User();
			user.setEmail(register.getEmail());
			user.setFirstName(register.getFirstName());
			user.setLastName(register.getLastName());
			user.setPassword(bCryptPasswordEncoder.encode(register.getPassword()));

			List<VerificationToken> verificationTokens = verificationTokenRepository.findByUserEmail(register.getEmail());
			VerificationToken verificationToken;
			if (verificationTokens.isEmpty()) {
				verificationToken = new VerificationToken();
				verificationToken.setUser(user);
				verificationTokenRepository.save(verificationToken);
			} else {
				verificationToken = verificationTokens.get(0);
			}

			return sendingMailService.sendVerificationMail(register.getEmail(), verificationToken.getToken());
		} catch (Exception e) {
			return null;
		}
		
	}

	public ResponseEntity<String> verifyEmail(String token) {
		List<VerificationToken> verificationTokens = verificationTokenRepository.findByToken(token);
		if (verificationTokens.isEmpty()) {
			return ResponseEntity.badRequest().body("Invalid token.");
		}

		VerificationToken verificationToken = verificationTokens.get(0);
		if (verificationToken.getExpiredDateTime().isBefore(LocalDateTime.now())) {
			return ResponseEntity.unprocessableEntity().body("Expired token.");
		}

		verificationToken.setConfirmedDateTime(LocalDateTime.now());
		verificationToken.setStatus(VerificationToken.STATUS_VERIFIED);
		verificationToken.getUser().setIsActive(true);
		verificationTokenRepository.save(verificationToken);

		return ResponseEntity.ok(
				"Hesabiniz Basariyla Dogrulanmistir.<br><a href=\"http://185.226.160.93:4200/#/dashboard/analytics\"\">Ana Sayfa</a>");
	}

}
