package com.mecbey.nemesisbackend.service.implement;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.MailException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.mecbey.nemesisbackend.dto.RegistrationRequest;
import com.mecbey.nemesisbackend.dto.UserDto;
import com.mecbey.nemesisbackend.entity.User;
import com.mecbey.nemesisbackend.entity.VerificationForm;
import com.mecbey.nemesisbackend.repository.UserRepository;
import com.mecbey.nemesisbackend.repository.VerificationTokenRepository;
import com.mecbey.nemesisbackend.service.NotificationService;
import com.mecbey.nemesisbackend.service.UserService;
import com.mecbey.nemesisbackend.util.TPage;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImplement implements UserService {

	private final UserRepository userRepository;
	private final ModelMapper modelMapper;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private VerificationTokenRepository verificationTokenRepository;

	@Autowired
	private NotificationService notificationService;

	public UserServiceImplement(UserRepository userRepository, ModelMapper modelMapper,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		this.modelMapper = modelMapper;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public UserDto save(User user) {
		UserDto u = modelMapper.map(user, UserDto.class);
		user = userRepository.save(user);
		u.setId(user.getId());
		return u;
	}

	@Override
	public UserDto getById(Long id) {
		User u = userRepository.getOne(id);
		return modelMapper.map(u, UserDto.class);
	}

	@Override
	public List<User> getByFirstNameAndLastName(String firstName, String lastName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getByFirstNameContains(String firstName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getByLastNameContains(String lastName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TPage<UserDto> getAllPageable(Pageable pageable) {
		Page<User> data = userRepository.findAll(pageable);
		TPage<UserDto> response = new TPage<UserDto>();
		response.setStat(data, Arrays.asList(modelMapper.map(data.getContent(), UserDto[].class)));
		return response;
	}

	@Override
	public Boolean delete(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	public Boolean delete(Long id) {
		userRepository.deleteById(id);
		return null;
	}

	@Override
	public UserDto update(Long id, User user) {
		User userDb = userRepository.getOne(id);
		if (userDb == null) {
			throw new IllegalArgumentException("User does not exist id: " + id);
		}

		User userCheck = userRepository.findByEmail(user.getEmail());

		if (userCheck != null) {
			throw new IllegalArgumentException("User already exist");
		}

		userDb.setFirstName(user.getFirstName());
		userDb.setLastName(user.getLastName());
		userDb.setEmail(user.getEmail());

		userRepository.save(userDb);
		return modelMapper.map(userDb, UserDto.class);
	}

	@Transactional
	public Boolean register(RegistrationRequest registrationRequest) {
		try {
			
			if(verificationTokenRepository.findByUserEmail(registrationRequest.getEmail()) == null) {
				return Boolean.FALSE;
			} else {
				User user = new User();
				user.setFirstName(registrationRequest.getFirstName());
				user.setLastName(registrationRequest.getLastName());
				user.setPassword(bCryptPasswordEncoder.encode(registrationRequest.getPassword()));
				userRepository.save(user);
				return Boolean.TRUE;
			}
			
			//notificationService.sentNotification(user);
			
		} catch (Exception e) {
			log.error("REGISTRATION=>", e);
			return Boolean.FALSE;
		}
	}

}
