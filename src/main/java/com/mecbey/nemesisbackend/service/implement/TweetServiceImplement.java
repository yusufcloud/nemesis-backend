package com.mecbey.nemesisbackend.service.implement;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.mecbey.nemesisbackend.entity.Tweets;
import com.mecbey.nemesisbackend.repository.TweetRepository;
import com.mecbey.nemesisbackend.service.TweetService;

@Service
public class TweetServiceImplement implements TweetService {
	
	private final TweetRepository tweetRepository;
	
	public TweetServiceImplement(TweetRepository tweetRepository) {
		this.tweetRepository = tweetRepository;
	}
	
	@Override
	public Tweets getTweet(String twitName) {
		return tweetRepository.getByName(twitName);
	}

}
