package com.mecbey.nemesisbackend.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.mecbey.nemesisbackend.dto.UserDto;
import com.mecbey.nemesisbackend.entity.User;
import com.mecbey.nemesisbackend.util.TPage;

public interface UserService {
	
	UserDto save(User user);

	UserDto getById(Long id);
	
	List<User> getByFirstNameAndLastName(String firstName, String lastName);
	
	User getByFirstNameContains(String firstName);
	
	User getByLastNameContains(String lastName);

	TPage<UserDto> getAllPageable(Pageable pageable);
	
	Boolean delete(User user);
	
	UserDto update(Long id, User user);

}
