package com.mecbey.nemesisbackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.mecbey.nemesisbackend.entity.User;

@Service
public class NotificationService {
	
	private JavaMailSender javaMailSender;
	
	@Autowired
	public NotificationService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
	
	public void sentNotification(User user) throws MailException {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(user.getEmail());
		mail.setFrom("mecbeyazilim@gmail.com");
		mail.setSubject("Nemesis Kayıdınız Başarıyla Alınmıştır");
		mail.setText("Kaydınız Başarıyla Alındı.");
		
		javaMailSender.send(mail);
	}

}
